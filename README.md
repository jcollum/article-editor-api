# Article Site API

Supports article creation. Three endpoints:

1. `/article/` - Get a list of articles
1. `/article/{id}` - Get an article.
1. `/article/new` - Create an article. Requires `title`, `body` and CSV `categories`

## Starting

1. Clone the repo
1. `npm install` (this also initializes the db)
1. `npm start`

## Integration testing

Real basic right now:

Get an article: `curl http://localhost:3000/article/Ikqx519ISE`

Post a new article: `curl -X POST http://localhost:3000/article/new -F title="foo" -F body="All foo all the tiiiiime" -F "categories=Art,Entertainment"`
