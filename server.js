const Hapi = require('@hapi/hapi');
const low = require('lowdb');
const get = require('lodash.get');
const FileSync = require('lowdb/adapters/FileSync');
const shortid = require('shortid');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ^$');

const adapter = new FileSync('./db/db.json');
const db = low(adapter);
const log = console.log;

//2. POST article:​ accepts form data and returns a success message along with some processed information (an ID is generated, as well as creation time)

// build routes
const routes = [
  {
    method: 'GET',
    path: '/article/',
    desc: 'Get articles',
    config: {
      cors: true
    },
    handler: async (request, h) => {
      log('Getting article list');
      const list = await db.get('articles').value();
      return list.map(x => ({ id: x.id, title: x.title }));
    }
  },
  {
    method: 'GET',
    path: '/article/{id}',
    desc: 'Get a single article',
    config: {
      cors: true
    },
    handler: async (request, h) => {
      const { id } = request.params;
      log('Getting article for id', id);
      const articles = await db.get('articles');
      const article = articles.find({ id }).value();
      if (!article) return h.response('Article not found').code(404);
      else return article;
    }
  },
  {
    method: 'POST',
    path: '/article/new',
    desc: 'Create an article',
    config: {
      cors: true
    },
    handler: async (request, h) => {
      try {
        const { title, body, categories } = JSON.parse(get(request, 'payload', '{}'));
        if (!(title && body && categories)) {
          log('Invalid input', { title: !!title, body: !!body, categories: !!categories });
          return h.response('Title, body and categories is required').code(400);
        }
        log(`Creating article `, { title, body, categories });
        const articles = await db.get('articles');
        const time = new Date();
        const id = shortid();
        articles
          .push({
            id: shortid(),
            title,
            body,
            categories: categories,
            timestamp: time
          })
          .write();
        log('Created new article, id:', id);
        return h.response({ id, time }).code(201);
      } catch (error) {
        log(error);
        return error;
      }
    }
  },
  {
    method: 'GET',
    path: '/health/',
    desc: 'Check server health',
    config: {
      cors: true
    },
    handler: (request, h) => {
      return 'OK';
    }
  }
];

// start the server
const init = async () => {
  const server = Hapi.server({
    port: 3000,
    host: 'localhost'
  });

  server.route(
    routes.map(x => {
      const { method, path, handler, config } = x;
      return { method, path, handler, config };
    })
  );
  await server.start();

  // log the routing table
  log('*** Routes ***');
  routes
    .map(x => ({ method: x.method, path: x.path, desc: x.desc }))
    .forEach(x => log('  ', x.path, '-', x.method, '-', x.desc));
  log('*** end ***');
  log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  log(err);
  process.exit(1);
});

init();
