// this script is used to initialize the file database if one is not present

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const defaults = require('./defaults.json');
const adapter = new FileSync('./db/db.json');
const db = low(adapter);

db.defaults(defaults).write();

console.log('Database initialized');
